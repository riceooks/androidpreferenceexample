package tw.edu.hust.yn97000.preference;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	
	Button btn_write, btn_reader, btn_clear;
	
	SharedPreferences preferences;
	SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // 宣告元件
        btn_write = (Button) findViewById(R.id.btn_write);
        btn_reader = (Button) findViewById(R.id.btn_reader);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        
        // 點擊事件
        btn_write.setOnClickListener(this);
        btn_reader.setOnClickListener(this);
        btn_clear.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		switch( v.getId() ){
		case R.id.btn_write:
			/*
			 *  以記錄名稱產生(取得)Preference
			 *  MODE_PRIVATE = 私有資料，只允許本身應用程式訪問
			 */
	        preferences = getSharedPreferences("example", MODE_PRIVATE);
	        
	        // 取得編輯器
	        editor = preferences.edit();
	        
	        // 堆入資料至編輯器
	        editor.putString("Name", "Barry");
	        editor.putInt("Value", 24);
	        
	        // 確認動作
	        editor.commit();
			break;
		case R.id.btn_reader:
			// 以記錄名稱產生(取得)Preference
			preferences = getSharedPreferences("example", MODE_PRIVATE);
			
			// 顯示資料
			Toast.makeText(this, preferences.getString("Name", "No string"), Toast.LENGTH_SHORT).show();
	        Toast.makeText(this, preferences.getInt("Value", 0) + "", Toast.LENGTH_SHORT).show();
			break;
		case R.id.btn_clear:
			// 以記錄名稱產生(取得)Preference
			preferences = getSharedPreferences("example", MODE_PRIVATE);

	        // 取得編輯器
	        editor = preferences.edit();
	        
			// 清除資料
	        editor.clear();
			
	        // 確認動作
	        editor.commit();
			break;
		}
	}

    
}
